import { NestKubemqOptions } from './interfaces';

import { NEST_KUBEMQ_OPTIONS } from './constants';

export function createNestKubemqProviders(
  options: NestKubemqOptions,
) {
  return [
    {
      provide: NEST_KUBEMQ_OPTIONS,
      useValue: options,
    },
  ];
}
