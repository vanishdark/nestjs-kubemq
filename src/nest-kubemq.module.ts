import { Module, DynamicModule, Provider, Global } from '@nestjs/common';
import { NestKubemqService } from './nest-kubemq.service';
import {
  NEST_KUBEMQ_OPTIONS,
} from './constants';
import {
  NestKubemqOptions,
  NestKubemqAsyncOptions,
  NestKubemqOptionsFactory,
} from './interfaces';
import { createNestKubemqProviders } from './nest-kubemq.providers';

@Global()
@Module({
  providers: [NestKubemqService],
  exports: [NestKubemqService],
})
export class NestKubemqModule {
  /**
   * Registers a configured NestKubemq Module for import into the current module
   */
  public static register(
    options: NestKubemqOptions,
  ): DynamicModule {
    return {
      module: NestKubemqModule,
      providers: createNestKubemqProviders(options),
    };
  }

  /**
   * Registers a configured NestKubemq Module for import into the current module
   * using dynamic options (factory, etc)
   */
  public static registerAsync(
    options: NestKubemqAsyncOptions,
  ): DynamicModule {
    return {
      module: NestKubemqModule,
      providers: [
        ...this.createProviders(options),
      ],
    };
  }

  private static createProviders(
    options: NestKubemqAsyncOptions,
  ): Provider[] {
    if (options.useExisting || options.useFactory) {
      return [this.createOptionsProvider(options)];
    }

    return [
      this.createOptionsProvider(options),
      {
        provide: options.useClass,
        useClass: options.useClass,
      },
    ];
  }

  private static createOptionsProvider(
    options: NestKubemqAsyncOptions,
  ): Provider {
    if (options.useFactory) {
      return {
        provide: NEST_KUBEMQ_OPTIONS,
        useFactory: options.useFactory,
        inject: options.inject || [],
      };
    }

    // For useExisting...
    return {
  provide: NEST_KUBEMQ_OPTIONS,
      useFactory: async (optionsFactory: NestKubemqOptionsFactory) =>
        await optionsFactory.createNestKubemqOptions(),
      inject: [options.useExisting || options.useClass],
    };
  }

 }
