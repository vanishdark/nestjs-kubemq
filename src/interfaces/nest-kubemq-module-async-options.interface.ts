/* Dependencies */
import { ModuleMetadata, Type } from '@nestjs/common/interfaces';

/* Interfaces */
import {
  NestKubemqOptions,
} from './nest-kubemq-options.interface';
import {
  NestKubemqOptionsFactory,
} from './nest-kubemq-options-factory.interface';

export interface NestKubemqAsyncOptions
  extends Pick<ModuleMetadata, 'imports'> {
  inject?: any[];
  useExisting?: Type<NestKubemqOptionsFactory>;
  useClass?: Type<NestKubemqOptionsFactory>;
  useFactory?: (
    ...args: any[]
  ) => Promise<NestKubemqOptions> | NestKubemqOptions;
}