/**
 *  NestKubemqClientModule is a testing module that verifies that
 *  NestKubemqModule was generated properly.
 *
 *  You can quickly verify this by running `npm run start:dev`, and then
 *  connecting to `http://localhost:3000` with your browser.  It should return
 *  a custom message like `Hello from NestKubemqModule`.
 *
 *  Once you begin customizing NestKubemqModule, you'll probably want
 *  to delete this module.
 */
import { Module } from '@nestjs/common';
import { NestKubemqClientController } from './nest-kubemq-client.controller';
import { NestKubemqModule } from '../nest-kubemq.module';

@Module({
  controllers: [NestKubemqClientController],
  imports: [NestKubemqModule.register({})],
})
export class NestKubemqClientModule {}
