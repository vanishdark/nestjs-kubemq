/**
 *  NestKubemqClientController is a testing controller that verifies that
 *  NestKubemqModule was generated properly.
 *
 *  You can quickly verify this by running `npm run start:dev`, and then
 *  connecting to `http://localhost:3000` with your browser.  It should return
 *  a custom message like `Hello from NestKubemqModule`.
 *
 *  Once you begin customizing NestKubemqModule, you'll probably want
 *  to delete this controller.
 */
import { Controller, Get } from '@nestjs/common';
import { NestKubemqService } from '../nest-kubemq.service';

@Controller()
export class NestKubemqClientController {
  constructor(private readonly nestKubemqService: NestKubemqService) {}

  @Get()
  index() {
    return this.nestKubemqService.test();
  }
}
