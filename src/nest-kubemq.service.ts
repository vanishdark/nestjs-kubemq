// tslint:disable: variable-name
import { Injectable, Inject, Logger } from '@nestjs/common';
import { NEST_KUBEMQ_OPTIONS} from './constants';
import { NestKubemqOptions } from './interfaces';

/**
 * Sample interface for NestKubemqService
 *
 * Customize this as needed to describe the NestKubemqService
 *
 */
interface INestKubemqService {
  test(): Promise<any>;
}

@Injectable()
/**
 *  You can remove the dependencies on the Logger if you don't need it.  You can also
 *  remove the `async test()` method.
 *
 *  The only thing you need to leave intact is the `@Inject(NEST_KUBEMQ_OPTIONS) private _nest-kubemqOptions`.
 *
 *  That injected dependency gives you access to the options passed in to
 *  NestKubemqService.
 *
 */
export class NestKubemqService implements INestKubemqService {
  private readonly logger: Logger;
  constructor(
    @Inject(NEST_KUBEMQ_OPTIONS) private _NestKubemqOptions: NestKubemqOptions,
  ) {
    this.logger = new Logger('NestKubemqService');
    this.logger.log(`Options: ${JSON.stringify(this._NestKubemqOptions)}`);
  }

  async test(): Promise<any> {
    return 'Hello from NestKubemqModule!';
  }
}