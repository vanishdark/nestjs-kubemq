import { NestKubemqOptions } from './interfaces';
/**
 * Sample interface for NestKubemqService
 *
 * Customize this as needed to describe the NestKubemqService
 *
 */
interface INestKubemqService {
    test(): Promise<any>;
}
export declare class NestKubemqService implements INestKubemqService {
    private _NestKubemqOptions;
    private readonly logger;
    constructor(_NestKubemqOptions: NestKubemqOptions);
    test(): Promise<any>;
}
export {};
