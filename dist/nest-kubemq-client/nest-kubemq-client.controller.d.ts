import { NestKubemqService } from '../nest-kubemq.service';
export declare class NestKubemqClientController {
    private readonly nestKubemqService;
    constructor(nestKubemqService: NestKubemqService);
    index(): Promise<any>;
}
