"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const constants_1 = require("./constants");
function createNestKubemqProviders(options) {
    return [
        {
            provide: constants_1.NEST_KUBEMQ_OPTIONS,
            useValue: options,
        },
    ];
}
exports.createNestKubemqProviders = createNestKubemqProviders;
