import { NestKubemqOptions } from './nest-kubemq-options.interface';
export interface NestKubemqOptionsFactory {
    createNestKubemqOptions(): Promise<NestKubemqOptions> | NestKubemqOptions;
}
