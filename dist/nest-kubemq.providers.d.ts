import { NestKubemqOptions } from './interfaces';
export declare function createNestKubemqProviders(options: NestKubemqOptions): {
    provide: string;
    useValue: NestKubemqOptions;
}[];
