import { DynamicModule } from '@nestjs/common';
import { NestKubemqOptions, NestKubemqAsyncOptions } from './interfaces';
export declare class NestKubemqModule {
    /**
     * Registers a configured NestKubemq Module for import into the current module
     */
    static register(options: NestKubemqOptions): DynamicModule;
    /**
     * Registers a configured NestKubemq Module for import into the current module
     * using dynamic options (factory, etc)
     */
    static registerAsync(options: NestKubemqAsyncOptions): DynamicModule;
    private static createProviders;
    private static createOptionsProvider;
}
